#!/usr/bin/env python
# -*- coding: utf-8 -*-
import asyncio
import socketio
import argparse
from dahua_rpc import DahuaRpc
from datetime import datetime

ap = argparse.ArgumentParser()
ap.add_argument("-u", "--username", type=str, default="root", help="auth username")
ap.add_argument("-p", "--password", type=str, default="admin", help="auth password")
ap.add_argument("-H", "--host", type=str, default="192.168.202.10", help="hostname")
ap.add_argument("-P", "--port", type=str, default="4080", help="port")
ap.add_argument("-l", "--logger", type=bool, default=True, help="debug logger")
args = vars(ap.parse_args())

sio = socketio.AsyncClient(engineio_logger=args["logger"])
AUTH = False
DAHUA_RUN = True
COUNT = 1000


async def sort_data(res, start_time, end_time, camera):
    """ формировка данных json """
    data = {"result": {"data": []}}
    if res['result']:
        found = res['params']['found']
        for counter in range(0, found):
            # добвить нужные параметры если нужно
            plate_num = res['params']['infos'][counter]['Summary']['TrafficCar']['PlateNumber']
            snapshot_time = res['params']['infos'][counter]['StartTime']
            url = "http://{}/cgi-bin/RPC_Loadfile{}".format(camera,
                                                            res['params']['infos'][counter]['FilePath'])
            country = res['params']['infos'][counter]['Summary']['TrafficCar']['Country']
            speed = res['params']['infos'][counter]['Summary']['TrafficCar']['Speed']
            size = res['params']['infos'][counter]['Summary']['TrafficCar']['VehicleSize']
            data['result']['data'].append(
                {'snapshotId': counter, 'plateNum': plate_num, 'snapshotTime': snapshot_time,
                 'country': country, 'speed': speed, 'vehicleSize': size,
                 'snapshotURL': url})
    else:
        found = 0
    data.update({"found": found})
    data.update({"camera": camera})
    data.update({"Start": start_time})
    data.update({"End": end_time})
    return data


async def send_message(data):
    """ отпрака дынных """
    await sio.emit('trigger.event', data)
    # import json
    # print(json.dumps(data, indent=2))


async def serv(dahua_data, dahua_auth, timer):
    while DAHUA_RUN:
        print(f'\nОтправка данных через {timer} секунд')
        start_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        await asyncio.sleep(timer)
        end_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if not DAHUA_RUN:
            break
        for num in range(0, len(dahua_data['camera'])):
            dahua = DahuaRpc(host=dahua_data['camera'][num]['hostname'],
                             username=dahua_data['camera'][num]['login'],
                             password=dahua_data['camera'][num]['password'])
            dahua_auth.append(dahua.login())
            if dahua_auth[num]:
                data = await sort_data(dahua.find_media_files(start=start_time, end=end_time, count=COUNT),
                                       start_time, end_time, dahua_data['camera'][num]['hostname'])
                # send data to server
                await send_message(data)
                dahua.logout()
                del dahua


@sio.event
async def get_by_timer(dahua_data, timer=300):
    """ запрос данных с камеры по таймеру """
    global DAHUA_RUN
    DAHUA_RUN = True
    dahua_auth = []
    if AUTH:
        for num in range(0, len(dahua_data['camera'])):
            print('Попытка подключение к камере {}'.format(dahua_data['camera'][num]['hostname']))
            dahua = DahuaRpc(host=dahua_data['camera'][num]['hostname'],
                             username=dahua_data['camera'][num]['login'],
                             password=dahua_data['camera'][num]['password'])
            dahua_auth.append(dahua.login())
            if dahua_auth[num]:
                print('Подключене c камерой {} установлено'.format(dahua_data['camera'][num]['hostname']))
                dahua.logout()
                del dahua
                await serv(dahua_data, dahua_auth, timer)
            else:
                print('Подключене c камерой {} НЕ установлено'.format(dahua_data['camera'][num]['hostname']))


@sio.event
async def stop_timer():
    """ остановить отправку данных с камеры по таймеру"""
    await send_message({'result': 'OK'})
    global DAHUA_RUN
    DAHUA_RUN = False


@sio.event
async def get_by_datetime(dahua_data, count, start_time="2020-01-01 00:00:00",
                          end_time=datetime.now().strftime("%Y-%m-%d %H:%M:%S")):
    """ запрос с указанием времени начала и конца поиска """
    if AUTH:
        for num in range(0, len(dahua_data['camera'])):
            print('Попытка подключение к камере {}'.format(dahua_data['camera'][num]['hostname']))
            dahua = DahuaRpc(host=dahua_data['camera'][num]['hostname'],
                             username=dahua_data['camera'][num]['login'],
                             password=dahua_data['camera'][num]['password'])
            logged = dahua.login()
            if logged:
                print('Подключене c камерой {} установлено'.format(dahua_data['camera'][num]['hostname']))
                data = await sort_data(dahua.find_media_files(start=start_time, end=end_time, count=count),
                                       start_time, end_time, dahua_data['camera'][num]['hostname'])
                await send_message(data)
                dahua.logout()
                del dahua
            else:
                print('Подключене c камерой {} НЕ установлено'.format(dahua_data['camera'][num]['hostname']))


# ----------------------------------------------------------------------------------------------------------------------
@sio.event
async def get_by_param(dahua_data, plate_number='*', start_time="2020-01-01 00:00:00",
                       end_time=datetime.now().strftime("%Y-%m-%d %H:%M:%S"), count=COUNT,
                       speed=None, country=None, vehicle_size=None):
    """ запрос с указанием определенного номера машины в указанном промежутке времени"""
    if AUTH:
        for num in range(0, len(dahua_data['camera'])):
            print('Попытка подключение к камере {}'.format(dahua_data['camera'][num]['hostname']))
            dahua = DahuaRpc(host=dahua_data['camera'][num]['hostname'],
                             username=dahua_data['camera'][num]['login'],
                             password=dahua_data['camera'][num]['password'])
            logged = dahua.login()
            if logged:
                print('Подключене c камерой {} установлено'.format(dahua_data['camera'][num]['hostname']))
                data = await sort_data(dahua.find_media_files(start=start_time, end=end_time,
                                                              count=count, plate_number=plate_number),
                                       start_time, end_time, dahua_data['camera'][num]['hostname'])
                await send_message(data)

                dahua.logout()
                del dahua
            else:
                print('Подключене c камерой {} НЕ установлено'.format(dahua_data['camera'][num]['hostname']))
# ----------------------------------------------------------------------------------------------------------------------


@sio.event
async def connect():
    await sio.emit('account.login', {'auth': {'username': args["username"],
                                              'password': args["password"]}}, callback=test_err)
    await asyncio.sleep(1)
    if AUTH:
        print('Подключене c сервером установлено')

        # await get_by_param(dahua_data={'camera': [{'hostname': "10.20.15.197",
        #                                            'login': "admin",
        #                                            'password': "view2020"}]}
        #                    , start_time="2021-02-08 08:00:00", end_time="2021-02-08 08:42:00")


async def test_err(err, _):
    """ проверка авторизаций """
    global AUTH
    if err is not None:
        print(err)
        await sio.disconnect()
    else:
        print('Авторизовано')
        AUTH = True


@sio.event
async def connect_error(msg):
    """ ошибка подключения """
    global AUTH
    AUTH = False
    print('Сообщения об ошибке подключения: {}'.format(msg))


@sio.event
async def disconnect():
    global AUTH
    AUTH = False
    print('Связь с сервером прервана')


async def start_server():
    """ подключение к серверу """
    print("Попытка подключения к серверу {}:{}".format(args["host"], args["port"]))
    await sio.connect("http://{}:{}".format(args["host"], args["port"]), transports='websocket')
    await sio.wait()


if __name__ == '__main__':
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(start_server())
    except Exception as e:
        print("Возникло исключение: {}".format(e))

# ------------------------------------------------------------------------------------------------------------------
